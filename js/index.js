"use strict";

const arr = [1, 78, "USA", undefined, "12345678"];

function filterBy(array, dataType) {
  const res = [];

  array.forEach((element) => {
    if (typeof element !== dataType) {
      console.log(element);
      res.push(element);
    }
  });
  return res;
}

console.log(filterBy(arr, "string"));
